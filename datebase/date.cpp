#include "date.h"

// конструктор выбрасывает исключение, если его аргументы некорректны
Date::Date(int new_year, int new_month, int new_day) {
    year = new_year;
    if (new_month > 12 || new_month < 1) {
        throw std::logic_error("Month value is invalid: " + std::to_string(new_month));
    }
    month = new_month;
    if (new_day > 31 || new_day < 1) {
        throw std::logic_error("Day value is invalid: " + std::to_string(new_day));
    }
    day = new_day;
}

Date::Date(const Date& date) {
    year = date.GetYear();
    month = date.GetMonth();
    day = date.GetDay();
}

int Date::GetYear() const {
    return year;
}

int Date::GetMonth() const {
    return month;
}

int Date::GetDay() const {
    return day;
}

// определить сравнение для дат необходимо для использования их в качестве ключей словаря
bool operator<(const Date &lhs, const Date &rhs) {
    // воспользуемся тем фактом, что векторы уже можно сравнивать на <:
    // создадим вектор из года, месяца и дня для каждой даты и сравним их
    return std::vector<int>{lhs.GetYear(), lhs.GetMonth(), lhs.GetDay()} <
           std::vector<int>{rhs.GetYear(), rhs.GetMonth(), rhs.GetDay()};
}

//
bool operator<=(const Date &lhs, const Date &rhs) {
    // воспользуемся тем фактом, что векторы уже можно сравнивать на <=:
    // создадим вектор из года, месяца и дня для каждой даты и сравним их
    return std::vector<int>{lhs.GetYear(), lhs.GetMonth(), lhs.GetDay()} <=
           std::vector<int>{rhs.GetYear(), rhs.GetMonth(), rhs.GetDay()};
}

bool operator>(const Date &lhs, const Date &rhs) {
    // воспользуемся тем фактом, что векторы уже можно сравнивать на >:
    // создадим вектор из года, месяца и дня для каждой даты и сравним их
    return std::vector<int>{lhs.GetYear(), lhs.GetMonth(), lhs.GetDay()} >
           std::vector<int>{rhs.GetYear(), rhs.GetMonth(), rhs.GetDay()};
}

bool operator>=(const Date &lhs, const Date &rhs) {
    // воспользуемся тем фактом, что векторы уже можно сравнивать на >=:
    // создадим вектор из года, месяца и дня для каждой даты и сравним их
    return std::vector<int>{lhs.GetYear(), lhs.GetMonth(), lhs.GetDay()} >=
           std::vector<int>{rhs.GetYear(), rhs.GetMonth(), rhs.GetDay()};
}


bool operator==(const Date &lhs, const Date &rhs) {
    // воспользуемся тем фактом, что векторы уже можно сравнивать на ==:
    // создадим вектор из года, месяца и дня для каждой даты и сравним их
    return std::vector<int>{lhs.GetYear(), lhs.GetMonth(), lhs.GetDay()} ==
           std::vector<int>{rhs.GetYear(), rhs.GetMonth(), rhs.GetDay()};
}

bool operator!=(const Date &lhs, const Date &rhs) {
    // воспользуемся тем фактом, что векторы уже можно сравнивать на !=:
    // создадим вектор из года, месяца и дня для каждой даты и сравним их
    return std::vector<int>{lhs.GetYear(), lhs.GetMonth(), lhs.GetDay()} !=
           std::vector<int>{rhs.GetYear(), rhs.GetMonth(), rhs.GetDay()};
}

// даты будут по умолчанию выводиться в нужном формате
std::ostream& operator<<(std::ostream &stream, const Date &date) {
    stream << std::setw(4) << std::setfill('0') << date.GetYear() <<
           "-" << std::setw(2) << std::setfill('0') << date.GetMonth() <<
           "-" << std::setw(2) << std::setfill('0') << date.GetDay();
    return stream;
}

std::ostream& operator<<(std::ostream& stream, const std::pair<Date, std::string>& entries) {
    stream << entries.first << ' ' << entries.second;
    return stream;
}

// парсим строковую дату в класс Date
Date ParseDate(std::istream& date_stream) {
    bool ok = true;

    int year;
    ok = ok && (date_stream >> year);
    ok = ok && (date_stream.peek() == '-');
    date_stream.ignore(1);

    int month;
    ok = ok && (date_stream >> month);
    ok = ok && (date_stream.peek() == '-');
    date_stream.ignore(1);

    int day;
    ok = ok && (date_stream >> day);

    if (!ok) {
        throw std::logic_error("Wrong date format!");
    }
    return Date(year, month, day);
}
