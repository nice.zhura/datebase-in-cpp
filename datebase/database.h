#ifndef DATABASE_H
#define DATABASE_H

#include <vector>
#include <set>
#include <map>

#include "events.h"
#include "date.h"

class Database {
private:
    std::map<Date, Events> storage;

    // Print events according data
    void PrintDateEvents(std::ostream& ostream, const Date& date) const;

public:
    // Add event into db
    void Add(const Date& date, const std::string& event);

    // Print all events in db
    void Print(std::ostream& ostream) const;

    // Get last element according to data
    std::string Last(const Date& date) const;

    template<class Predicate>
    int RemoveIf(Predicate predicate) {
        int count = 0;

        std::vector<Date> to_clean;

        for (auto& item : storage) {
            // get event log
            auto events = item.second.GetEventLog();

            // get events to clean
            std::vector<std::string> events_to_clean;
            std::copy_if(std::begin(events), std::end(events),
                         std::back_inserter(events_to_clean),
                         [=](const std::string& event) {
                return predicate(item.first, event);
            });

            if (events_to_clean.empty()) {
                continue;
            }

            for (const auto& event_to_clean : events_to_clean) {
                count += item.second.RemoveEvent(event_to_clean);
            }

            if (item.second.Empty()) {
                to_clean.push_back(item.first);
            }
        }

        // Clear storage without events
        for (const auto& item_to_clean : to_clean) {
            storage.erase(item_to_clean);
        }

        return count;
    }

    template<class Predicate>
    Entries FindIf(Predicate predicate) const {
        Entries entries;

        for (const auto& item : storage) {
            auto events = item.second.GetEventLog();
            for (const auto& event : events) {
                if (predicate(item.first, event)) {
                    entries.template emplace_back(std::make_pair(item.first, event));
                }
            }
        }
        return entries;
    }

};

#endif //DATABASE_H
