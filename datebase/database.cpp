#include "database.h"

void Database::Add(const Date &date, const std::string &event) {
    if (storage.count(date) != 0) {
        Events& events = storage.at(date);
        if (events.CountEvent(event) != 0) {
            return;
        }
        events.AddEvent(event);
    }
    storage[date].AddEvent(event);
}

void Database::PrintDateEvents(std::ostream& ostream, const Date& date) const {
    auto events = storage.at(date).GetEventLog();
    for (const auto& event : events) {
        ostream << date << ' ' << event << std::endl;
    }
}

void Database::Print(std::ostream &ostream) const {
    for (const auto& item : storage) {
        PrintDateEvents(ostream, item.first);
    }
}

std::string Database::Last(const Date &date) const {
    if (storage.empty()) {
        throw std::invalid_argument("Empty database");
    }

    auto it = storage.lower_bound(date);
    if (it == storage.begin() && date < it->first) {
        throw std::invalid_argument("No entries for requested date");
    }

    if (it == storage.end() || it->first != date) {
        it = std::prev(it);
    }

    std::ostringstream oss {};
    oss << it->first << ' ' << it->second.GetLastEvent();

    return oss.str();
}
