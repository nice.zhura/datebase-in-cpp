#include "events.h"

void Events::AddEvent(const std::string &event) {
    if (event_list.find(event) == event_list.end()) {
        event_list.insert(event);
        event_log.emplace_back(event);
    }
}

size_t Events::CountEvent(const std::string &event) const {
    return event_list.count(event);
}

bool Events::Empty() const {
    return event_list.empty();
}

const std::set<std::string>& Events::GetEventList() const {
    return event_list;
}

const std::list<std::string>& Events::GetEventLog() const {
    return event_log;
}

std::string Events::GetLastEvent() const {
    if (!event_log.empty()) {
        return event_log.back();
    }
    throw std::runtime_error("event_log is empty in GetLastEvent method!");
}

int Events::RemoveEvent(const std::string &event) {
    auto it = std::find(std::begin(event_log), std::end(event_log), event);
    if (it != event_log.end()) {
        event_log.erase(it);
        event_list.erase(event);
        return 1;
    }
    return 0;
}
