#ifndef DATE_H
#define DATE_H

#include <stdexcept>
#include <iostream>
#include <sstream>
#include <iomanip>
#include <string>
#include <vector>

class Date {
public:
    // конструктор выбрасывает исключение, если его аргументы некорректны
    Date(int new_year, int new_month, int new_day);
    Date(const Date& date);

    int GetYear() const;
    int GetMonth() const;
    int GetDay() const;

private:
    int year;
    int month;
    int day;
};

using Entries = std::vector<std::pair<Date, std::string>>;

// определить сравнение для дат необходимо для использования их в качестве ключей словаря
bool operator<(const Date& lhs, const Date& rhs);
bool operator<=(const Date &lhs, const Date &rhs);

bool operator>(const Date &lhs, const Date &rhs);
bool operator>=(const Date &lhs, const Date &rhs);

bool operator==(const Date &lhs, const Date &rhs);
bool operator!=(const Date &lhs, const Date &rhs);

// даты будут по умолчанию выводиться в нужном формате
std::ostream& operator<<(std::ostream& stream, const Date& date);

std::ostream& operator<<(std::ostream& stream, const std::pair<Date, std::string>& entries);

// парсим строковую дату в класс Date
Date ParseDate(std::istream& istream);

#endif // DATE_H
