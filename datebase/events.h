#ifndef EVENTS_H
#define EVENTS_H

#include <stdexcept>
#include <algorithm>
#include <string>
#include <list>
#include <set>

class Events {
private:
    std::set<std::string> event_list;
    std::list<std::string> event_log;
public:
    // Add event into event list and event log
    void AddEvent(const std::string& event);

    // Count number of event in event list
    size_t CountEvent(const std::string& event) const;

    // Check if event list is empty
    bool Empty() const;

    // Get event list by const reference
    const std::set<std::string>& GetEventList() const;
    const std::list<std::string>& GetEventLog() const;

    // Get last event
    std::string GetLastEvent () const;

    // Remove event
    int RemoveEvent(const std::string& event);
};

#endif //EVENTS_H
