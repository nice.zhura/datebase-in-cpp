#include "node.h"

bool EmptyNode::Evaluate(const Date &date, const std::string &event) const {
    return true;
}

bool DateComparisonNode::Evaluate(const Date &date, const std::string &event) const {
    return CompareObjects(date, date_, cmp_);
}

bool EventComparisonNode::Evaluate(const Date &date, const std::string &event) const {
    return CompareObjects(event, event_, cmp_);
}

bool LogicalOperationNode::Evaluate(const Date &date, const std::string &event) const {
    if (op_ == LogicalOperation::And) {
        return lhs_->Evaluate(date, event) && rhs_->Evaluate(date, event);
    } else if (op_ == LogicalOperation::Or) {
        return lhs_->Evaluate(date, event) || rhs_->Evaluate(date, event);
    } else {
        throw std::invalid_argument("Invalid logical operation");
    }
}
