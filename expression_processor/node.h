#ifndef NODE_H
#define NODE_H

#include <memory>
#include "../datebase/date.h"

// Create Node class
class Node {
public:
    virtual bool Evaluate(const Date& date, const std::string& event) const = 0;
};

// Implement Empty Node
class EmptyNode : public Node {
    bool Evaluate(const Date& date, const std::string& event) const override;
};

// Starting implementing DateComparisonNode, EventComparisonNode

// Create Comparison enum for implementation of operations
enum class Comparison {
    Less, LessOrEqual,
    Greater, GreaterOrEqual,
    Equal, NotEqual
};

// Create template compare method
template<typename T>
bool CompareObjects(const T& lhs, const T& rhs, const Comparison& cmp) {
    if (cmp == Comparison::Less) {
        return lhs < rhs;
    } else if (cmp == Comparison::LessOrEqual) {
        return lhs <= rhs;
    } else if (cmp == Comparison::Greater) {
        return lhs > rhs;
    } else if (cmp == Comparison::GreaterOrEqual) {
        return lhs >= rhs;
    } else if (cmp == Comparison::Equal) {
        return lhs == rhs;
    } else if (cmp == Comparison::NotEqual) {
        return lhs != rhs;
    } else {
        throw std::invalid_argument("Invalid comparison operator");
    }
}

// Create DateComparisonNode class
class DateComparisonNode : public Node {
private:
    const Comparison cmp_;
    const Date date_;
public:
    DateComparisonNode(const Comparison& cmp, const Date& date)
    : cmp_(cmp), date_(date) {}

    bool Evaluate(const Date& date, const std::string& event) const override;

};

// Create EventComparisonNode class
class EventComparisonNode : public Node {
private:
    const Comparison cmp_;
    const std::string event_;
public:
    EventComparisonNode(const Comparison& cmp, const std::string& event)
    : cmp_(cmp), event_(event) {}

    bool Evaluate(const Date& date, const std::string& event) const override;
};

// Starting implementing LogicalOperationNode

// Create LogicalOperation enum for logic operations implementation
enum class LogicalOperation {
    Or, And
};

// Create LogicalOperationNode class
class LogicalOperationNode : public Node {
private:
    const LogicalOperation op_;
    const std::shared_ptr<Node> lhs_, rhs_;
public:
    LogicalOperationNode(const LogicalOperation& op,std::shared_ptr<Node> lhs, std::shared_ptr<Node> rhs)
    : op_(op), lhs_(lhs), rhs_(rhs) {}

    bool Evaluate(const Date& date, const std::string& event) const override;
};

#endif //NODE_H
